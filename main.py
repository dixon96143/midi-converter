import midi
import sys
import fileWriter
import argsParser
import numberToNoteParser


if __name__ == "__main__":
    args = argsParser.parse_args()
    filename = args['file']
    delta = args['tone'] or 0
    octave_switch = args['octave'] or False
    print("Script to convert midi to recorder fingering")

    song = midi.read_midifile(filename)
    song.make_ticks_abs()
    tracks = []
    notes_of_octave = []
    for track in song:
        notes = [note for note in track if note.name == 'Note On']
        pitch = [note.pitch for note in notes]
        for frq in pitch:
            note_tuple = numberToNoteParser.number_to_note(
                frq, delta,
                octave_switch)
            note = note_tuple[0]
            notes_of_octave.append(note_tuple)
    fileWriter.writeToFile(notes_of_octave, filename, 15)
