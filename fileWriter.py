import notesParser

symbols = ['note', 'thumb', 'left_1', 'left_2',
           'left_3', 'right_1', 'right_2', 'right_3', 'right_4']

fingering = notesParser.notes()


def split(arr, size):
    arrs = []
    while len(arr) > size:
        pice = arr[:size]
        arrs.append(pice)
        arr = arr[size:]
    arrs.append(arr)
    return arrs


def writeNotes(file, notes, symbol):
    for note in note:
        file.write(str(fingering[note][symbol]) + '    ')
    file.write('\n')


def writeNote(file, note):
    file.write(note[0] + str(note[1]) + '   ')


def writeLine(file, size):
    file.write('\n')
    file.write('-' * size * 5)


def writeChunk(note_chunk, file):
    for symbol in symbols:
        for note in note_chunk:
            if symbol == 'note':
                writeNote(file, note)
            else:
                write_fingering_for(note, symbol, file)
        if (symbol == 'thumb' or symbol == 'left_3'):
            writeLine(file, 15)
        file.write('\n')


def write_fingering_for(note, symbol, file):
    file.write(str(fingering[note][symbol]) + '    ')


def skip_three_lines(file):
    file.write('\n\n\n')


def writeToFile(notes, filename, notes_per_line=15):
    file = open(filename[:-4] + '.txt', "w")
    chunked_notes = split(notes, notes_per_line)
    for note_chunk in chunked_notes:
        writeChunk(note_chunk, file)
        skip_three_lines(file)
    file.close()
