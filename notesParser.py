import json


def parseNoteAndOctave(note_string):
    if len(note_string) == 2:
        tone = note_string[:1]
        octave = int(note_string[1:])
        return (tone, octave)
    else:
        tone = note_string[:2]
        octave = int(note_string[2:])
        return (tone, octave)


def notes(filename=None):
    if filename == None:
        filename = "notesFingering.json"
    with open(filename) as f:
        notes_fingering_json = json.load(f)
        note_fingering = {}
        for note in notes_fingering_json.keys():
            noteAndOctaveTuple = parseNoteAndOctave(note)
            note_fingering[noteAndOctaveTuple] = notes_fingering_json[note]
        return note_fingering
