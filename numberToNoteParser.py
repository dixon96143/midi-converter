# Return note tuple for number Example: 72 -> (C, 7); 74 -> (D, 7)
NOTES = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
OCTAVES = list(range(11))
NOTES_IN_OCTAVE = len(NOTES)


def number_to_note(number: int, delta: int = 2, octave_switch: bool = False) -> tuple:
    number = number + delta
    octave = number // NOTES_IN_OCTAVE
    assert octave in OCTAVES, "Bad input"
    assert 0 <= number <= 127, "Bad input"
    note = NOTES[number % NOTES_IN_OCTAVE]
    if octave_switch:
        if octave % 2 == 0:
            octave = 6
        else:
            octave = 7
    else:
        if octave % 2 == 0:
            octave = 7
        else:
            octave = 6
    return note, octave
