import argparse


def parse_args():
    parser = argparse.ArgumentParser(
        description='Process midi files and creates recorder fingering (soprano)')
    parser.add_argument('--file',
                        required=True,
                        help="Path to midi file ex: --file=drunken_sailor.mid")
    parser.add_argument('--tone',
                        type=int,
                        help="Tone delta. You can move midi up and down by passing integer here. Example if you pass 1, C -> C#, D -> D#...")
    parser.add_argument('--octave',
                        type=bool,
                        help="Flag to swith octaves of recorder. (6 <-> 7)")
    return vars(parser.parse_args())
